import requests
#comentario prueba
import json
from loguru import logger
from requests.auth import HTTPBasicAuth
from os import environ
from Crypto.Util.Padding import unpad
from Crypto.Cipher import AES
import base64

# Constants
CONTENT_TYPE = 'application/json'
URL_JIRA = 'https://alignet.atlassian.net'

# Enviroment
AES_MODE = int(environ.get('AES_MODE'))
KEY_DECODE = base64.b64decode(environ.get('KEY'))
IV_DECODE = base64.b64decode(environ.get('IV'))

# Variables
msg_success = []
msg_errors = []

def handler(event, context):
    
    logger.info("EVENT - {}",json.dumps(event))
    
    #Obtener issue key de pathParamenter
    issue_key = event['pathParameters']['key']
    logger.info('KEY - {}', issue_key)
    
    # Validar Headers (authorization, issuer-key)
    req_headers = event['headers']
    headers_lower = { k.lower(): v for k, v in req_headers.items() }
    error_header = get_required_headers(headers_lower)
    if error_header:
        prepared_body = get_prepared_body('update_issue', 'No se envió header(s) obligatorio(s)', False,  errors=error_header)
        logger.info(get_prepared_response(400, prepared_body))
        return get_prepared_response(400, prepared_body)
    
    # Obtener dtos de Header
    token = headers_lower['token']
    
    # Validar datos necesarios de body
    req_body = event['body']
    error_body = get_required_body(req_body)
    if error_body:
        prepared_body = get_prepared_body('update_issue', 'No se enviaron objetos en el body para su procesamiento.', False, errors=error_body)
        logger.info(get_prepared_response(400, prepared_body))
        return get_prepared_response(400, prepared_body)

    # Obtener datos del body
    body = json.loads(event['body'])
    logger.info("BODY - {}", json.dumps(body))
    body_lower = { k.lower(): v for k, v in body.items() }
    creator =body_lower['creator']

    # Desencryptar token
    token_decrypt = decrypt_token(token)

    if 'fields' in body_lower and body_lower['fields']:
        logger.info('Campos enviados en fields - {}', body_lower['fields'])
        body_requests(token_decrypt, creator, issue_key, body_lower['fields'])

    #Agregar elementos opcionales enviados en el request
    if 'comment' in body_lower and body_lower['comment']:
        post_comment_issue(token_decrypt, creator, issue_key, body_lower['comment'])
    
    if 'transitions' in body_lower and body_lower['transitions']:
        post_transitions_issue(token_decrypt, creator, issue_key, body_lower['transitions'])

    if 'issuelink' in body_lower and body_lower['issuelink']:
        post_issue_link(token_decrypt, creator, issue_key, body_lower['issuelink'])
    

    prepared_body = get_prepared_body("update_issue", "Se actualizó el Issue.", True, data=msg_success, errors=msg_errors, code='00')
    logger.info('RESPONSE_END_UPDATE_ISSUE - {}', prepared_body)

    return get_prepared_response(200, prepared_body)

def get_user_jira(token, creator, email_user):
    logger.info('Ingresó a la función de búsqueda de usuario')
    #Obtener el id del usuario de jira
    url_search_user= f'{URL_JIRA}/rest/api/3/user/search?query={email_user}'
    logger.info("URL_GET_USER - {}", url_search_user)
    response_get_user = requests.get(url_search_user, headers={'Accept': CONTENT_TYPE},auth=HTTPBasicAuth(creator, token))
    response_status_user = response_get_user.status_code
    logger.info("RESPONSE_STATUS_CODE_USER - {}", response_status_user)
    
    if response_status_user == 200:
        response_json_user = response_get_user.json()
        return response_json_user[0]['accountId']

def body_requests(token, creator, key, issue_fields):
    req_body = {
        "fields":{}
    }
    req_body.update (validar_fechas(issue_fields))
    post_issue_fields(token, creator, key, issue_fields,req_body)
    
def validar_fechas(issue_fields):
    dates= {"fields":{}}
    if 'datestart' in issue_fields and issue_fields['datestart']:
        print("contiene datestart")
        datestart= issue_fields['datestart']
        print("datestart enviado", datestart)
        dates['fields']['customfield_10531'] = datestart
    
    if 'dateend' in issue_fields and issue_fields['dateend']:
        print("contiene dateend")
        dateend= issue_fields['dateend']
        print("dateend enviado", dateend)
        dates['fields']['customfield_10575'] = dateend
    
    if 'duedate' in issue_fields and issue_fields['duedate']:
        print("contiene duedate")
        duedate= issue_fields['duedate']
        print("duedate enviado", duedate)
        dates['fields']['duedate'] = duedate
    
    if 'baselinestart' in issue_fields and issue_fields['baselinestart']:
        print("contiene baselinestart")
        baselinestart= issue_fields['baselinestart']
        print("baselinestart enviado", baselinestart)
        dates['fields']['customfield_10576'] = baselinestart
        
    if 'baselineend' in issue_fields and issue_fields['baselineend']:
        print("contiene baselineend")
        baselineend= issue_fields['baselineend']
        print("baselineend enviado", baselineend)
        dates['fields']['customfield_10577'] = baselineend
        
    if 'registerdate' in issue_fields and issue_fields['registerdate']:
        print("contiene registerdate")
        registerdate= issue_fields['registerdate']
        print("registerdate enviado", registerdate)
        dates['fields']['customfield_10567'] = registerdate
    logger.info("dates - {}", dates)
    return dates
   
def post_issue_fields(token, creator, key, issue_fields,req_body):
    #Obtener campos de body
    logger.info('Ingresó a la función de actualización de campos de Issue')
    url_update_fields = f'{URL_JIRA}/rest/api/3/issue/{key}'
    logger.info('URL_UPDATE_FIELDS - {}', url_update_fields)
    
    if 'assignee' in issue_fields and issue_fields['assignee']:
        accound_id = get_user_jira(token, creator, issue_fields['assignee'])
        req_body['fields']['assignee'] = {"id": accound_id}
        
    if 'labels' in issue_fields and issue_fields['labels']:
        print("contiene labels")
        label= issue_fields['labels']
        print("label enviado", label)
        req_body['fields']['labels'] = [label]

    if 'priority' in issue_fields and issue_fields['priority']:
        print("contiene prioridad")
        priority= issue_fields['priority']
        print("prioridad enviada", priority)
        req_body['fields']['priority'] = { 'id': priority }
        
    if 'epiclink' in issue_fields and issue_fields['epiclink']:
        print("contiene epiclink")
        epiclink= issue_fields['priority']
        print("prioridad enviada", epiclink)
        req_body['fields']['customfield_10018'] = epiclink

    logger.info('Ingresó a la función de actualización prioridad')
    url_update_fields = f'{URL_JIRA}/rest/api/3/issue/{key}'
    logger.info('URL_UPDATE_FIELDS - {}', url_update_fields)
        
    logger.info('REQUEST_BODY_UPDATE_FIELDS - {}', json.dumps(req_body))
    response_update_fields = requests.put(url_update_fields, data=json.dumps(req_body),headers={'Accept': CONTENT_TYPE, 'Content-Type': CONTENT_TYPE}, auth=HTTPBasicAuth(creator, token))
    status_code_update_fields = response_update_fields.status_code
    logger.info("RESPONSE_STATUS_CODE_USER - {}", status_code_update_fields)
    
    if status_code_update_fields != 204:
        response_json = response_update_fields.json()
        logger.info("RESPONSE_ERROR_UPDATE_ISSUE - {}", response_json)
        msg_errors.append('Error al actualizar campos del Issue.')
    else:
        # No envía response body
        msg_success.append('Se actualizaron campos del Issue.')
    return req_body
def post_issue_link(token, creator, key, issue_link):
    logger.info('Ingresó a la función de vinculación de Issue')
    for item in issue_link:
        if 'check' in item and item['check'] == True:
            url_issue_link = f'{URL_JIRA}/rest/api/3/issueLink'
            logger.info("URL_TRANSITION_ISSUE - {}", url_issue_link)
            req_body = {
                "outwardIssue": {
                    "key": item['key']
                },
                "inwardIssue": {
                    "key": key
                },
                "type": {
                    "name": "Blocks" #GET - /rest/api/3/issue/{issue-key}/transitions
                }
            }
            logger.info("BODY_ISSUE_LINK - {}", json.dumps(req_body))
            response_issue_link = requests.post(url_issue_link, data=json.dumps(req_body),headers={'Accept': CONTENT_TYPE, 'Content-Type': CONTENT_TYPE},auth=HTTPBasicAuth(creator, token))
            status_code_issue_link = response_issue_link.status_code
            logger.info("STATUS_CODE_ISSUE_LINK - {}", status_code_issue_link)

            # Devuelve 201 si la solicitud es exitosa.
            if status_code_issue_link != 201:
                response_json = response_issue_link.json()
                logger.info("RESPONSE_ERROR_ISSUE_LINK - {}", response_json)
                msg_errors.append("Error al vincular Issue '{}'.".format(item['key']))
            
            else:
                # No envía response body
                msg_success.append("Se creó la vinculación del Issue '{}'.".format(item['key']))
    
def post_transitions_issue(token, creator,key, id_transition):
    logger.info('Ingresó a la función de cambio de estado para un issue')
    url_transition = f'{URL_JIRA}/rest/api/3/issue/{key}/transitions'
    logger.info("URL_TRANSITION_ISSUE - {}", url_transition)
    req_body = {
        "transition": {
            "id": str(id_transition) # 11: "Por hacer", 21: "En curso", 31: "Listo", 41: "Bloqueado"
        }
    }
    logger.info("BODY_TRANSITION_ISSUE - {}", json.dumps(req_body))
    response_transition = requests.post(url_transition, data=json.dumps(req_body),headers={'Accept': CONTENT_TYPE, 'Content-Type': CONTENT_TYPE},auth=HTTPBasicAuth(creator, token))
    status_code_transition = response_transition.status_code
    logger.info("STATUS_CODE_TRANSITION_ISSUE - {}", status_code_transition)

    # Devuelve 204 si la solicitud es exitosa.
    if status_code_transition != 204:
        response_json = response_transition.json()
        logger.info("RESPONSE_ERROR_TRANSITION_ISSUE - {}", response_json)
        msg_errors.append('Error al actualizar el estado del Issue.')
    else:
        # No envía response body
        msg_success.append('Se acualizó el estado del Issue.')

def post_comment_issue(token, creator,key, coment):
    logger.info('Ingresó a la función de adición de comentario')
    url_issue_coment= f'{URL_JIRA}/rest/api/3/issue/{key}/comment'
    logger.info("URL_ADD_ISSUE_COMMENT - {}", url_issue_coment)
    req_body = {
        "body": {
            "type": "doc",
            "version": 1,
            "content": [
                {
                    "type": "paragraph",
                    "content": [
                        {
                            "text": coment,
                            "type": "text"
                        }
                    ]
                }
            ]
        }
    }
    logger.info("BODY_ADD_ISSUE_COMMENT - {}", json.dumps(req_body))
    response_issue_comment = requests.post(url_issue_coment, data=json.dumps(req_body),headers={'Accept': CONTENT_TYPE, 'Content-Type': CONTENT_TYPE},auth=HTTPBasicAuth(creator, token))
    response_status_issue_comment = response_issue_comment.status_code
    logger.info("STATUS_CODE_ADD_ISSUE_COMMENT - {}", response_status_issue_comment)

    # Devuelve 201 si la solicitud es exitosa.
    if response_status_issue_comment != 201:
        response_json = response_issue_comment.json()
        logger.info("RESPONSE_ERROR_ADD_ISSUE_COMMENT - {}", response_json)
        msg_errors.append('Error al agregar el comentario en el Issue.')
    else:
        msg_success.append('Se agregó exitosamente el comentario.')
          
def get_required_headers(headers_request):
    errors = []
    template = {'token': 'str'}
    for key in template:
        if key not in headers_request:
            errors.append(f"No se envió el header '{key}'.")
            continue
        if not headers_request[key]:
            errors.append(f"No se envió valor para el header '{key}'")
    return errors

def get_required_body(body_request):
    errors = []
    if not body_request:
        errors.append( "No de envió campo 'creator'")
        return errors
    body_request = json.loads(body_request)
    body_lower = { k.lower(): v for k, v in body_request.items()}
    template = {'creator': 'str'}
    for key in template:
        if key not in body_lower:
            errors.append(f"No se envió el header '{key}'.")
            continue
        if not body_lower[key]:
            errors.append(f"No se envió valor para el header '{key}'")
    return errors

def decrypt_token(token):
    decrypt_data = None
    try:
        decode_base64 = base64.b64decode(token)
        cipher2 = AES.new(KEY_DECODE, AES_MODE, IV_DECODE)
        decrypt = unpad(cipher2.decrypt(decode_base64), 16)
        decrypt_data = decrypt.decode('UTF-8')
    except Exception as e:
        logger.info('Error al desencriptar token - {}', e)
    return decrypt_data

def get_prepared_body(action, msg, success, data=[], errors=[], code='01'):
    payload={
        "action": action,
        "success": success,
        "data": data,
        "errors": errors,
        "meta": {
            "status": {
                "code": code,
                "message_ilgn": [{
                    "locale": "es_PE",
                    "value": msg
                }]
            }
        }
    }
    return payload

def get_prepared_response(status_code, prepared_body):
    response = {
        'statusCode': status_code,
        'headers': {
            'Content-Type': CONTENT_TYPE,
            'Access-Control-Allow-Origin': '*'
        },
        'body': json.dumps(prepared_body)
    }
    return response